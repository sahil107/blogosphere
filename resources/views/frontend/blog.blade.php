@extends('frontend.layout.app')

@section('content')

<!-- Blog Area
        ===================================== -->
<section id="blog" class="pt75 pb50">
    <div class="container">

        <div class="row">
            <div class="col-md-9">

                <div class="blog-three-mini">
                    <h2 class="color-dark">
                        <a href="#">{{ $blog->title}}</a>
                    </h2>
                    <div class="blog-three-attrib">
                        <div><i class="fa fa-calendar"></i>{{ $blog->published_at->diffForHumans()}}</div> |
                        <div><i class="fa fa-pencil"></i><a href="#">{{$blog->author->name}}</a></div> |
                        <div><i class="fa fa-comment-o"></i>
                            <a href="{{ route('frontend.category', $blog->category->id)}}">
                                {{ $blog->category->name}}
                            </a>
                        </div>
                         |
                        <div><a href="#"><i class="fa fa-thumbs-o-up"></i></a>150 Likes</div> |
                        <div>
                            Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>

                    <img src="{{asset($blog->image_path)}}" alt="Blog Image" class="img-responsive">
                    {!! $blog->body !!}

                    <div class="blog-post-read-tag mt50">
                        <i class="fa fa-tags"></i> Tags:
                        @foreach ($blogTags as $tag)
                            <a href="{{ route('frontend.tag', $tag->id)}}">{{ $tag->name}}</a>,
                        @endforeach
                    </div>
                </div>


                <div class="blog-post-author mb50 pt30 bt-solid-1">
                    <img src="{{asset('assets/img/other/photo-1.jpg')}}" class="img-circle" alt="image">
                    <span class="blog-post-author-name">{{ $blog->author->name}}</span> <a href="https://twitter.com"><i class="fa fa-twitter"></i></a>
                    <p>
                        About Author : Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
                    </p>
                </div>
            </div>
            @include('frontend.layout._sidebar')
        </div>
        {{-- COMMENT SECTION --}}

        <div class="blog-post-comment-container">
            <h5><i class="fa fa-comments-o mb25"></i> {{count($comments)}} Comments</h5>


            {{-- COMMENT --}}

            @foreach ($comments as $comment)
                <div class="blog-post-comment">
                    <span class="blog-post-comment-name" style="font-weight: 800;">{{$comment->name}}</span>  {{ $comment->published_at}}

                    <a href="#" class="pull-right text-gray"><i class="fa fa-comment"></i> Reply</a>

                    <p>
                        {{$comment->comment}}
                    </p>
                </div>
            @endforeach
            {{-- <div class="blog-post-comment">
                <img src="assets/img/other/photo-4.jpg" class="img-circle" alt="image">
                <span class="blog-post-comment-name">John Boo</span> Jan. 20 2016, 10:00 PM
                <a href="#" class="pull-right text-gray"><i class="fa fa-comment"></i> Reply</a>
                <p>
                    Adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.
                </p>

                {{-- COMMENT REPLY --}}
            {{--    <div class="blog-post-comment-reply">
                    <img src="assets/img/other/photo-2.jpg" class="img-circle" alt="image">
                    <span class="blog-post-comment-name">John Boo</span> Jan. 20 2016, 10:00 PM
                    <a href="#" class="pull-right text-gray"><i class="fa fa-comment"></i> Reply</a>
                    <p>
                        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.
                    </p>
                </div>

            </div> --}}

            {{-- <button class="button button-default button-sm center-block button-block mt25 mb25">Load More Comments</button> --}}


        </div>
        @include('admin.layout._alert-messages')
        <div class="blog-post-leave-comment">
            <h5><i class="fa fa-comment mt25 mb25"></i> Leave Comment</h5>

            <form action="{{ route('admin.comments.store', $blog->id)}}" method="POST">
                @csrf
                <input type="text" name="name" class="blog-leave-comment-input" placeholder="Enter your Name" value="{{ auth()->user() ? auth()->user()->name : ''}}" required>
                <input type="email" name="email" class="blog-leave-comment-input" placeholder="Enter your Email" value="{{ auth()->user() ? auth()->user()->email : ''}}"  required>
                <textarea name="comment" class="blog-leave-comment-textarea" value="{{old('comment')}}" placeholder="Write a comment!"></textarea>

                <input type="hidden" name="blog_id" value="{{$blog->id}}">
                @if (auth()->user())
                    <button type="submit" class="button button-pasific button-sm center-block mb25">Leave Comment</button>
                @else
                    <p class="text-center text-danger" style="font-size: 1.5rem; font-weight: 3px">Please Login in to Comment on this blog!</p>
                @endif

            </form>

        </div>

        </div>

        {{-- <div id="disqus_thread"></div>
            <script>
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
                /*
                var disqus_config = function () {
                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                */
                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://blogosphere-3.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div> --}}

</section>
@endsection
