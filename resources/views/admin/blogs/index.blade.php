@extends('admin.layout.app')

@section('main-content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Blogs</h1>
            <a href="{{route('admin.blogs.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">                <i class="fas fa-plus fa-sm text-white-50"></i>
             Create Blog</a>
        </div>
    <!-- End Page Heading -->

    <div class="row">
        <div class="col-md-12">

            @include('admin.layout._alert-messages')

            <table class="table table-bordered table-responsive">
                <thead>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Excerpt</th>
                    <th>Category</th>
                    <th>Actions</th>
                    @if (auth()->user()->isAdmin())
                    <th>Approve</th>
                    @endif
                    {{-- @if (auth()->user()->isOwner($blog))
                    <th>Publish Status</th>
                    @endif --}}


                </thead>
                <tbody>
                    @foreach ($blogs as $blog)
                        <tr>
                            <td>{{ $blog->id }}</td>
                            <td>
                                <img src="{{asset($blog->image_path)}}" alt="{{$blog->title}}" width="100px">
                            </td>
                            <td>{{$blog->title}}</td>
                            <td>{{$blog->excerpt}}</td>
                            <td>{{$blog->category->name}}</td>
                            <td>
                                <a href="{{ route('admin.blogs.edit', $blog->id)}}" class="btn btn-primary">
                                    <i class="fas fa-pen"></i>
                                </a>
                                <button class="btn btn-danger mt-1" data-toggle="modal" data-target="#deleteModal"
                                onclick="deleteModalHelper('{{route('admin.blogs.trash', $blog->id)}}' )">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                            @if (auth()->user()->isAdmin())
                            <td>
                                @if ($blog->isApproved === 1)
                                    <button class="" data-toggle="modal" data-target="#unapproveModal"
                                    onclick="unApproveModalHelper('{{route('admin.blogs.unapprove', $blog->id)}}' )" style="border: none; background-color: #fff">
                                    <i class="fa fa-toggle-on" aria-hidden="true" style="color: green; font-size: 30px"></i>
                                    </button>
                                @else
                                    <button class="" data-toggle="modal" data-target="#approveModal"
                                    onclick="approveModalHelper('{{route('admin.blogs.approve', $blog->id)}}' )" style="border: none; background-color: #fff">
                                    <i class="fa fa-toggle-off" aria-hidden="true" style="color: green; font-size: 30px"></i>
                                    </button>
                                @endif
                            </td>
                            @elseif (auth()->user()->isOwner($blog))
                            <td>
                                @if ($blog->isPublished === 1)
                                    <div class="text-center">
                                        <button class="" data-toggle="modal" data-target="#unpublishModal"
                                        onclick="unpublishModalHelper('{{route('admin.blogs.unpublish', $blog->id)}}' )" style="border: none; background-color: #fff">
                                        <i class="fa fa-toggle-on" aria-hidden="true" style="color: green; font-size: 30px"></i>
                                        </button>
                                        <br>
                                        Published
                                    </div>
                                @else
                                    <div class="text-center">
                                        <button class="" data-toggle="modal" data-target="#publishModal"
                                        onclick="publishModalHelper('{{route('admin.blogs.publish', $blog->id)}}' )" style="border: none; background-color: #fff">
                                        <i class="fa fa-toggle-off" aria-hidden="true" style="color: green; font-size: 30px"></i>
                                        </button>
                                        Unpublished
                                    </div>
                                @endif
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if ($blogs->count() == 0)
                <p>No Blogs Found!</p>
            @endif
            {{ $blogs->links('vendor.pagination.bootstrap-5')}}
        </div>
    </div>
</div>

 <!-- Approve Modal-->
 <div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="approveModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="approveForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="approveModalLabel">Approve this Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to approve this blog?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success" type="submit">Approve</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- unApprove Modal-->
 <div class="modal fade" id="unapproveModal" tabindex="-1" role="dialog" aria-labelledby="unapproveModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="unapproveForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="unapproveModalLabel">Unapprove this Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to Unapprove this blog?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit">Unapprove</button>
                </div>
            </div>
        </form>
    </div>
</div>

 <!-- Publish Modal-->
 <div class="modal fade" id="publishModal" tabindex="-1" role="dialog" aria-labelledby="publishModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="publishForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="publishModalLabel">Publish this Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to publish this blog?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success" type="submit">Publish</button>
                </div>
            </div>
        </form>
    </div>
</div>

 <!-- Unpublish Modal-->
 <div class="modal fade" id="unpublishModal" tabindex="-1" role="dialog" aria-labelledby="unpublishModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="unpublishForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="unpublishModalLabel">Unpublish this Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to Unpublish this blog?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit">Unpublish</button>
                </div>
            </div>
        </form>
    </div>
</div>
 <!-- DELETE Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="deleteForm">
            @csrf
            @method('DELETE')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to delete this blog?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit">Delete</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection


@section('scripts')
<script>
    function deleteModalHelper(url){
        document.getElementById("deleteForm").setAttribute('action', url);
    }
    function approveModalHelper(url){
        document.getElementById("approveForm").setAttribute('action', url);
    }
    function unApproveModalHelper(url){
        document.getElementById("unapproveForm").setAttribute('action', url);
    }
    function publishModalHelper(url){
        document.getElementById("publishForm").setAttribute('action', url);
    }
    function unpublishModalHelper(url){
        document.getElementById("unpublishForm").setAttribute('action', url);
    }



</script>
@endsection

