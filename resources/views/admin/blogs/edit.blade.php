@extends('admin.layout.app')

@section('styles')
    {{-- SELECT2 FROM DROPDOWN --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    {{-- FLATPICKR FOR DATE --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    {{-- TRIX FOR EDITOR , TEXT AREA --}}
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/trix@2.0.0/dist/trix.css">
@endsection



@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script type="text/javascript" src="https://unpkg.com/trix@2.0.0/dist/trix.umd.min.js"></script>
    <script>
        $(".select2").select2({
            placeholder: "Select a value",
            allowClear: true
        });

        flatpickr("#published_at", {
            enableTime: true,
            altInput: true,
            altFormat: "F j, Y H:i",
            dateFormat: "Y-m-d H:i",
        });
    </script>
@endsection

@section('main-content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Blog</h1>
        </div>
    <!-- End Page Heading -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action=" {{ route('admin.blogs.update', $blog->id)}}" method="POST" enctype="multipart/form-data"> {{--  --}}
                        @csrf
                        @method('PUT')

                        {{-- Title --}}
                        <div class="form-group">
                            <label for="title" class="form-label">Title</label>
                            <input type="text"
                            class="form-control @error('title') border-design text-danger @enderror"
                            value="{{old('title', $blog->title)}}"
                            placeholder="Enter Title"
                            id="title"
                            name="title">
                            @error('title')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of Title --}}

                        {{-- Excerpt --}}
                        <div class="form-group">
                            <label for="excerpt" class="form-label">Excerpt</label>
                            <input type="text"
                            class="form-control @error('excerpt') border-design text-danger @enderror"
                            value="{{old('excerpt', $blog->excerpt)}}"
                            placeholder="Enter Excerpt"
                            id="excerpt"
                            name="excerpt">
                            @error('excerpt')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of Excecrpt --}}

                        {{-- Body --}}
                        <div class="form-group">
                            <label for="body" class="form-label">Body of the Blog</label>
                            <input type="hidden"
                            class="form-control @error('body') border-design text-danger @enderror"
                            value="{{old('body', $blog->body)}}"
                            placeholder="Enter Body"
                            id="body"
                            name="body">
                        <trix-editor input="body"></trix-editor>

                            @error('body')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of Body --}}

                        {{-- Category Id --}}
                        <div class="form-group">
                            <label for="category_id" class="form-label">Category</label>
                            <select name="category_id" id="category_id" class="form-control select2">
                                <option></option>
                                @foreach ($categories as $category)
                                    @if ($category->id == old('category_id', $blog->category_id))
                                        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>

                            @error('category_id')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of Category Id --}}

                        {{-- Tags --}}
                        <div class="form-group">
                            <label for="tags" class="form-label">Tags</label>
                            <select name="tags[]" id="tags" class="form-control select2" multiple>
                                <option></option>
                                @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}"
                                        {{ in_array($tag->id, old('tags', $blog->tags()->pluck('tags.id')->toArray())) ? 'selected' : '' }}>

                                            {{ $tag->name }}
                                        </option>
                                @endforeach
                            </select>
                            @error('tags')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of Tags--}}

                        {{-- Published_at --}}
                        <div class="form-group">
                            <label for="published_at" class="form-label">Tags</label>
                            <input type="date"
                            value="{{ old('published_at', $blog->published_at) }} "
                            class="form-control @error('published_at') border-danger text-danger @enderror"
                            placeholder="Enter Published Date"
                            id="published_at"
                            name="published_at">
                            @error('published_at')
                                <span class="text-danger">
                                    {{ $message }}  <!-- $message will show all the required validations together-->
                                </span>
                            @enderror
                        </div>
                        {{-- End of published_at--}}

                        {{-- Image file --}}
                        <div>
                            <div class="col-md-4">
                                <img src="{{asset($blog->image_path)}}" alt="" width="100%">
                            </div>
                            <div class="mt-2"> </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="image_path" class="form-label">Image</label>
                                    <input type="file"
                                    class="form-control @error('image_path') border-danger text-danger @enderror"
                                    placeholder="Select Image File"
                                    id="image_path"
                                    name="image">
                                    @error('image')
                                        <span class="text-danger">
                                            {{ $message }}  <!-- $message will show all the required validations together-->
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- End of Image file--}}

                        <div class="form-group mt-3 ">
                            <button type="submit "class="btn btn-primary" name="addBlog">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
