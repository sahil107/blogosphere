@extends('admin.layout.app')

@section('main-content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Users</h1>
            <a href="{{route('admin.blogs.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">                <i class="fas fa-plus fa-sm text-white-50"></i>
             Create Admin</a>
        </div>
    <!-- End Page Heading -->

    <div class="row">
        <div class="col-md-12">

            @include('admin.layout._alert-messages')

            <table class="table table-bordered table-responsive">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Is Verified</th>
                    <th>verify Email</th>
                    <th>Role</th>
                    <th>Actions</th>

                </thead>
                <tbody>
                    @foreach ($users as $user)

                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name}}</td>
                            <td>{{ $user->email}}</td>
                            <td>{{ $user->email_verified_at <= now() ? 'Verified' : '' }}</td>
                            <th class="text-center">
                                @if ($user->email_verified_at == null)
                                    <button class="btn btn-success" data-toggle="modal" data-target="#verifyModal"
                                    onclick="verifyModalHelper('{{route('admin.users.verify', $user->id)}}' )">
                                        <i class="fa fa-check"></i> Verify Now
                                    </button>
                                @else
                                Verified <i class="fa fa-check">
                                @endif
                            </th>
                            <td>{{ $user->role}}</td>
                            <td>
                                @if ($user->role === "author")
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#makeAdminModal"
                                    onclick="makeAdminModalHelper('{{route('admin.users.makeadmin', $user->id)}}' )">
                                        <i class="fa fa-user-plus"></i>
                                    </button> Make as Admin
                                @else
                                    <button class="btn btn-danger" data-toggle="modal" data-target="#revokeAdminModal"
                                    onclick="revokeAdminModalHelper('{{route('admin.users.revoke', $user->id)}}' )">
                                        <i class="fa fa-user-minus"></i>
                                    </button> Revoke as Admin
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if ($users->count() == 0)
                <p>No Users Found!</p>
            @endif
            {{ $users->links('vendor.pagination.bootstrap-5')}}
        </div>
    </div>
</div>

<!-- Verify Modal-->
<div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="verifyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="verifyForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyModalLabel">Verify User?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to verify this email?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success" type="submit">Verify</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Make Admin Modal-->
<div class="modal fade" id="makeAdminModal" tabindex="-1" role="dialog" aria-labelledby="makeAdminModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="makeAdminForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="makeAdminModalLabel">Make Admin?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to make this User an Admin?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Make Admin</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Revoke Admin Modal-->
<div class="modal fade" id="revokeAdminModal" tabindex="-1" role="dialog" aria-labelledby="revokeAdminModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="revokeAdminForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="revokeAdminModalLabel">Delete Blog?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to Revoke this User as Admin?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-danger" type="submit">Revoke</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection


@section('scripts')
<script>
    function verifyModalHelper(url){
        document.getElementById("verifyForm").setAttribute('action', url);
    }
    function makeAdminModalHelper(url){
        document.getElementById("makeAdminForm").setAttribute('action', url);
    }
    function revokeAdminModalHelper(url){
        document.getElementById("revokeAdminForm").setAttribute('action', url);
    }

</script>
@endsection

