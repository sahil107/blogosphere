@extends('admin.layout.app')

@section('main-content')
<div class="container-fluid">
    <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Comments</h1>
            {{-- <a href="{{route('admin.blogs.index')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">                <i class="fas fa-plus fa-sm text-white-50"></i>
             Create Admin</a> --}}
        </div>
    <!-- End Page Heading -->

    <div class="row">
        <div class="col-md-12">

            @include('admin.layout._alert-messages')

            <table class="table table-bordered table-responsive">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Comment</th>
                    <th>Blog Title</th>
                    <th>Actions</th>

                </thead>
                <tbody>
                    @foreach ($comments as $comment)

                        <tr>
                            <td>{{ $comment->id }}</td>
                            <td>{{ $comment->name}}</td>
                            <td>{{ $comment->email}}</td>
                            <td>{{ $comment->comment}}</td>
                            <td>{{ $comment->blog->title}}</td>
                            <td class="text-center">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#approveModal"
                                onclick="approveModalHelper('{{route('admin.comments.update', $comment->id)}}' )">
                                    <i class="fa fa-check"></i> Approve Now
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if ($comments->count() == 0)
                <p>No Comments Found!</p>
            @endif
            {{ $comments->links('vendor.pagination.bootstrap-5')}}
        </div>
    </div>
</div>

<!-- Approve Modal-->
<div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="approveModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="" id="approveForm">
            @csrf
            @method('PUT')
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="approveModalLabel">Verify User?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure you want to approve this comment?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-warning" type="submit">Approve</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection


@section('scripts')
<script>
    function approveModalHelper(url){
        document.getElementById("approveForm").setAttribute('action', url);
    }
</script>
@endsection

