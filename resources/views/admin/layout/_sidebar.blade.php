
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="brand-icon">
            <img src="C:/Users/Sahil Israni/Downloads/blogosphere.png" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">Blogosphere</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : ''}}">
        <a class="nav-link" href="{{ route('admin.dashboard')}}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Route::currentRouteName() == 'admin.blogs.index'? 'active' : ''}} {{ Route::currentRouteName() == 'admin.blogs.trashed' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fa fa-book"></i>
            <span>Blogs</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Blogs:</h6>
                <a class="collapse-item" href="{{ route('admin.blogs.index')}}">Active Blogs</a>
                <a class="collapse-item" href="{{ route('admin.blogs.trashed')}}">Trashed Blogs</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item {{ Route::currentRouteName() == 'admin.users.index' ? 'active' : ''}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fa fa-user-circle"></i>
            <span>Users</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Users:</h6>
                <a class="collapse-item" href="{{ route('admin.users.index')}}">User Table</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Others
    </div>

    <!-- Nav Item - Pages Collapse Menu -->

    <!-- Nav Item - Charts -->
    <li class="nav-item {{ Route::currentRouteName() == 'admin.categories.index' ? 'active' : ''}}">
        <a class="nav-link" href="{{route('admin.categories.index')}}">
            <i class="fa fa-bars"></i>
            <span>Categories</span></a>
    </li>

    <li class="nav-item {{ Route::currentRouteName() == 'admin.tags.index' ? 'active' : ''}}">
        <a class="nav-link" href="{{route('admin.tags.index')}}">
            <i class="fa fa-tags"></i>
            <span>Tags</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->

