<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for($i = 1 ; $i <= 10; $i++){
            $user = User::all()->random();
            $blog = Blog::all()->random();
            // $bloguser = User::all()->random();
            $comment = Comment::create([
                'comment' => fake()->sentence(),
                'name' => $user->name,
                'email' => $user->email,
                'published_at' => now(),
                'user_id'=>$user->id,
                'blog_id' =>$blog->id,
                'blog_user_id'=> $blog->user_id
            ]);
        }

    }
}
