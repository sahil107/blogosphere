<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::create([
            'name' => 'Sahil',
            'email' => 'sahil@gmail.com',
            'password' => Hash::make('abcd1234'),
            'role'=>'admin'
        ]);
        User::factory(10)->create();
        $categories = ['Sports', 'Technology', 'Gaming'];

        $tags = ['sports', 'technology', 'gaming', 'fun', 'electronics'];

        foreach($categories as $category){
            $user = User::all()->random();
            Category::create([
                'name' => $category,
                'created_by'=>$user->id,
                'last_updated_by'=>$user->id
            ]);

        }

        foreach($tags as $tag){
            $user = User::all()->random();
            Tag::create([
                'name' => $tag,
                'created_by' => $user->id,
                'last_updated_by' =>$user->id
            ]);
        }

        $this->call(BlogsSeeder::class);
        $this->call(CommentsSeeder::class);
    }
}
