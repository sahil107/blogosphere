<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\CreateBlogRequest;
use App\Http\Requests\Blogs\UpdateBlogRequest;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use Illuminate\Http\Request;

class BlogsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['validateAuthor'])->only(['edit', 'update', 'destroy', 'trash', 'publish', 'unpublish']);
        $this->middleware('validateAdmin')->only(['approve', 'unapprove']);
    }

    public function index()
    {
        $authUser = auth()->user();
        if($authUser->isAdmin()){
            $blogs = Blog::with('category')->latest()->paginate(10);  //latest() shows according the latest updated_at in the table!
        }
        else{
            $blogs = Blog::with('category')
                          ->where('user_id', $authUser->id)
                          ->latest()
                          ->paginate(10);
        }

        return view('admin.blogs.index', compact(['blogs']));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.blogs.create', compact(['categories', 'tags']));
    }

    public function store(CreateBlogRequest $request)
    {
        // dd($request->draft);
        //Image Upload and return the name of the file which will be created
        $image_path = $request->file('image')->store('blogs');
        $data = $request->only(['title', 'excerpt', 'body', 'category_id', 'published_at']);
        if($request->draft === null){
            $draft = 0;
            $publishedAt = null;

            $data = $request->only(['title', 'excerpt', 'body', 'category_id']);
            $data = array_merge($data, [
                'image_path'=>$image_path,
                'isPublished'=>$draft,
                'published_at' => $publishedAt,
                'isApproved' => 1,
                'user_id'=>auth()->user()->id
            ]);
        }
        else{
            $draft = 1;

            $data = $request->only(['title', 'excerpt', 'body', 'category_id', 'published_at']);
            $data = array_merge($data, [
                'image_path'=>$image_path,
                'isPublished'=>$draft,
                'isApproved' => 1,
                'user_id'=>auth()->user()->id
            ]);
        }
        $blog = Blog::create($data);
        $blog->tags()->attach($request->tags);

        session()->flash('success', 'Blog created successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function show(string $id)
    {
        //
    }

    public function edit(Blog $blog)
    {
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.blogs.edit', compact([
            'categories',
            'tags',
            'blog'
        ]));
    }

    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $data = $request->only(['title', 'excerpt', 'body', 'category_id', 'published_at']);

        if($request->hasFile('image')){
            $image_path = $request->file('image')->store('blogs');
            $blog->deleteImage();  //here deleteImage() is function we have created in Blog model!
            $data = array_merge($data, ['image_path'=>$image_path]);
        }

        $blog->update($data);
        $blog->tags()->sync($request->tags);

        session()->flash('success', 'Blog Updated successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function trash(Blog $blog){
        $blog->delete();  //here delete() functioin is overridden by the SoftDelete Class ka delete() function jaha deleted_at time is updated not the data is deleted!
        session()->flash('success', 'Blog Deleted successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function destroy(int $blogId)
    {
        $blog = Blog::onlyTrashed()->find($blogId);
        $blog->deleteImage();
        $blog->forceDelete();

        session()->flash('success', 'Blog Destroyed successfully...');
        return redirect(route('admin.blogs.trashed'));
    }
    public function trashed(){
        $blogs = Blog::with('category')
                      ->where('user_id', auth()->id())
                      ->onlyTrashed()
                      ->latest()
                      ->paginate(10);

        return view('admin.blogs.trashed', compact(['blogs']));
    }
    public function restore(int $blogId){
        $blog = Blog::withTrashed()->find($blogId);
        $blog->restore();

        session()->flash('success', 'Blog Restored successfully...');
        return redirect(route('admin.blogs.index'));
    }

    public function approve(Blog $blog){
        $blog->isApproved = 1;
        $blog->save();

        session()->flash('success', 'Blog Approved successfully...');
        return redirect(route('admin.blogs.index'));
    }
    public function unapprove(Blog $blog){
        $blog->isApproved = 0;
        $blog->save();

        session()->flash('success', 'Blog Unapproved successfully...');
        return redirect(route('admin.blogs.index'));
    }
    public function unpublish(Blog $blog){
        $blog->isPublished = 0;
        $blog->published_at = null;
        $blog->save();

        session()->flash('success', 'Blog Unpublished successfully...');
        return redirect(route('admin.blogs.index'));
    }
    public function publish(Blog $blog){
        $blog->isPublished = 1;
        $blog->published_at = now();
        $blog->save();

        session()->flash('success', 'Blog Published successfully...');
        return redirect(route('admin.blogs.index'));
    }

}
