<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
        $blogs = Blog::with('author')
                    ->where('isApproved', 1)
                    ->where('isPublished', 1)
                    ->search()
                    ->published()
                    ->orderBy('published_at', 'desc')
                    ->simplePaginate(3);
        $categories = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();

        return view('frontend.index', compact(['blogs', 'tags', 'categories']));
    }

    public function category(Category $category){
        $blogs =$category->blogs()->where('isApproved', 1)->where('isPublished', 1)->search()->published()->simplePaginate(3);
        $categories = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();
        return view('frontend.index', compact(['categories', 'tags', 'blogs']));
    }
    public function tag(Tag $tag){
        $blogs =$tag->blogs()->where('isApproved', 1)->where('isPublished', 1)->search()->published()->simplePaginate(3);
        $categories = Category::withCount('blogs')->get();
        $tags = Tag::limit(10)->get();
        return view('frontend.index', compact(['categories', 'tags', 'blogs']));
    }

    public function show(Blog $blog){
        $blogTags =$blog->tags;
        $categories = Category::withCount('blogs')->get();  #for sidebar
        $tags = Tag::limit(10)->get(); #for sidebar
        $comments = Comment::all()->where('blog_id', $blog->id)->where('approved_by', '!=', null);
        return view('frontend.blog', compact(['categories', 'tags', 'blog', 'blogTags', 'comments']));
    }


}
