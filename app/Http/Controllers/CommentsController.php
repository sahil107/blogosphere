<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function index(Blog $blogs)
    {
        if(auth()->user()->isAdmin()){
            $comments = Comment::with('blog')->where('approved_by', null)->paginate(10);
            return view('admin.comments.index', compact(['comments']));
        }

            $comments = Comment::with('blog')->where('blog_user_id', auth()->user()->id)->where('approved_by', null)->paginate(10);
            return view('admin.comments.index', compact(['comments']));
    }


    public function store(Request $request, Blog $blog)
    {
        // dd($blog, $request);
        $blogId = $request->blog_id;
        $s = Blog::where('id', $blogId)->first();
        // dd($s);
            Comment::create([
                'name' =>$request->name,
                'email' =>$request->email,
                'comment' =>$request->comment,
                'user_id'=> auth()->user()->id,
                'blog_id' => $request->blog_id,
                'blog_user_id'=> $s->user_id
            ]);
            session()->flash('success', 'Comment added successfully...');
            return redirect(route('frontend.blogs.show', $blogId));

    }
    public function update(Comment $comment){
        if($comment->approved_by !== null){
            session()->flash('error', 'Comment is approved already...');
            return redirect(route('admin.comments.index'));
        }
        $userName = auth()->user()->name;
        $comment->approved_by = $userName;
        $comment->save();

        session()->flash('success', 'Comment approved successfully...');
        return redirect(route('admin.comments.index'));
    }

}
