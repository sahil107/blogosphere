<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('validateAdmin')->only(['index', 'verify', 'admin', 'revoke']);
    }

    public function index()
    {
        $users = User::paginate(6);
        return view('admin.users.index', compact(['users']));
    }
    public function verify(User $user){
        // dd($user);
        $user->email_verified_at = now();
        $user->save();

        session()->flash('success', 'Email verified successfully...');
        return redirect(route('admin.users.index'));
    }
    public function admin(User $user){
        if(!$user->email_verified_at == null){
            $user->role = 'admin';
            $user->save();

            session()->flash('success', 'Category updated successfully...');
            return redirect(route('admin.users.index'));
        }
        else{
            session()->flash('error', 'You cannot make this user as Admin as its email is not verified yet!');
            return redirect(route('admin.users.index'));
        }
    }
    public function revoke(User $user){
        $userId = auth()->user()->id;
        if($userId === $user->id){
            session()->flash('error', 'You cannot revoke yourself as Admin!');
            return redirect(route('admin.users.index'));
        }
        else{
            $user->role = 'author';
            $user->save();
            session()->flash('success', 'Category updated successfully...');
            return redirect(route('admin.users.index'));
        }
    }

}
