<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag\CreateTagRequest;
use App\Http\Requests\Tags\UpdateTagRequest;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Cache\TagSet;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['validateAdmin'])->only(['create','edit', 'update', 'destroy','trashed', 'trash']);
    }

    public function index()
    {
        $tags = Tag::latest()->paginate(10);  //latest() shows according the latest updated_at in the table!
        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTagRequest $request)
    {
        // dd($request);
        // $data = request()->all();
        // request()->validate([
        //     'name'=> 'required|min:3|max:255'
        // ]);
        $userId = auth()->user()->id;
        Tag::create([
            'name' => $request->name,
            'created_by'=>$userId,
            'last_updated_by'=>$userId,
        ]);
        // session()->put('success', 'Tag created successfully...');
        session()->flash('success', 'Tag created successfully...');
        return redirect(route('admin.tags.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', compact(['tag']) );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag->name = $request->name;
        $tag->last_updated_by = auth()->user()->id;
        $tag->save();

        session()->flash('success', 'Tag updated successfully...');
        return redirect(route('admin.tags.index'));
    }

    public function destroy(Request $request, Tag $tag)
    {
        //TODO: validate if there are post associated with this category then dont delete. else proceed
        if($tag->blogs()->count() > 0){
            session()->flash('error', 'Tag cannot be deleted as it has blogs associated with it!');
            return redirect(route('admin.tags.index'));
        }

        $tag->delete();
        session()->flash('success', 'Tag Destroyed successfully!');
        return redirect(route('admin.tags.index'));
    }
}
