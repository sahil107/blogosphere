<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['validateAdmin'])->only(['create','edit', 'update', 'destroy']);
    }

    public function index()
    {

        $categories = Category::latest('updated_at')->paginate(5);  //latest() shows according the latest updated_at in the table!
        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        // dd($request);
        // $data = request()->all();
        // request()->validate([
        //     'name'=> 'required|min:3|max:255'
        // ]);
        $userId = auth()->user()->id;
        Category::create([
            'name' => $request->name,
            'created_by'=>$userId,
            'last_updated_by'=>$userId,
        ]);
        // session()->put('success', 'Category created successfully...');
        session()->flash('success', 'Category created successfully...');
        return redirect(route('admin.categories.index'));
    }

    public function show(string $id)
    {
        //
    }

    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact(['category']) );
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        $category->last_updated_by = auth()->user()->id;
        $category->save();

        session()->flash('success', 'Category updated successfully...');
        return redirect(route('admin.categories.index'));
    }

    public function destroy(Request $request, Category $category)
    {
        //TODO: validate if there are post associated with this category then dont delete. else proceed
        if($category->blogs()->count() > 0){
            session()->flash('error', 'Category cannot be deleted as it has blogs associated with it!');
            return redirect(route('admin.categories.index'));
        }

        $category->delete();
        session()->flash('success', 'Category Destroyed successfully!');
        return redirect(route('admin.categories.index'));
    }

}
